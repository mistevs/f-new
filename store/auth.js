import Me from '@/api/me'
import axios from 'axios'

export const state = () => ({
	permitted: false,
	password: {},
	me: {}
})

export const mutations = {
	setPermitted(state, password) {
		// console.log('set permitted')
		state.permitted = true
		state.password = password
	},

	setMe(state, me) {
		// console.log('state updated', me)
		state.me = me
		// this.$cookies.set('me', state.me)
	},

	setEvents(state, events) {
		console.log('setting events', events)
		state.me = { ...state.me, events }
		console.log('successfully set events', state.me)
	},

	logout(state) {
		state.me = {}
	}
}

export const actions = {
	permit({ commit }, password) {
		// console.log('calling permit', password)
		commit('setPermitted', password)
	},

	me({ commit }, me) {
		// console.log('setting me', me)
		commit('setMe', me)
	},

	async getEvents({ commit }) {
		console.log('getting events')
		let me = new Me()

		let events = await me.events()

		console.log('got events', events)

		commit('setEvents', events.data?.events)
	},

	setup({ dispatch }, token) {
		console.log('setting up auth', token)
		let me = new Me()

		if (token) {
			let expires = 'Jan 1, ' + (new Date().getFullYear() + 1)
			let removeResult = this.$cookies.remove('_t')
			console.log('setting token', token, removeResult, this.$cookies)
			this.$cookies.set('_t', token, {
				expires: new Date(expires)
			})
		}

		let _t = token || this.$cookies.get('_t')
		console.log('retrieving cookie for me', _t)
		if (_t) {
			let header = `Bearer ${_t}`
			// console.log('setting axios defeault auth header to', header)
			axios.defaults.headers.common['Authorization'] = header

			me.auth().then(data => {
				// console.log('authing me', data.data)
				dispatch('me', {
					...data.data.user,
					events: data.data.events
				})
			})
		}
	},

	logout({ commit }) {
		// console.log('logging out')

		this.$cookies.remove('_t')
		commit('logout')
	}
}

export const getters = {
	permitted(state) {
		// console.log('getting permitted', state.permitted)
		return state.permitted
	}
}
