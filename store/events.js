export const state = () => ({
	// events: {
	// 	'f2019' : {
	// 		title : 'F 2019',
	// 	}
	// },
	// event: {}
})

export const mutations = {
	setEvent(state, eventCode) {
		state.event = state.events[eventCode]
	}
}

export const actions = {
	getEvent({ commit }, eventCode) {
		commit('setEvent', eventCode)
	}
}
