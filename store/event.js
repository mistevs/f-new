import Vue from 'vue'

export const state = () => ({
	permitted: {}
})

export const mutations = {
	setPermitted(state, event) {
		Vue.set(state.permitted, event, true) //state.permitted[event] = true
		console.log('set permitted', state.permitted)
	}
}

export const actions = {
	permit({ commit }, event) {
		// console.log('calling permit', password)
		commit('setPermitted', event)
	}
}

export const getters = {
	permitted: state => event => {
		console.log('getting permitted', event, state.permitted[event])
		return state.permitted[event]
	}
}
