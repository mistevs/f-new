// import Vuex from 'vuex'

export const state = () => ({
	notifications: [],
	cacheVersion: ''
})

export const mutations = {
	setCacheVersion(state, version) {
		state.cacheVersion = version
	},
	setNotification(state, notification) {
		// console.log('set notification', notification)
		state.notifications.push(notification)
	},

	removeNotification(state, message) {
		// console.log('removing notification', message, state.notifications)
		const newN = state.notifications.filter(n => n.message !== message)
		state.notifications = newN
	}
}

export const actions = {
	loadCacheVersion({ commit }) {
		return this.$storyapi.get(`cdn/spaces/me`).then(res => {
			commit('setCacheVersion', res.data.space.version)
		})
	},

	notify({ commit }, { type, message }) {
		// console.log('notifying', type, message)
		commit('setNotification', { type: type || 'error', message })

		setTimeout(() => {
			commit('removeNotification', message)
		}, 4500)
	},

	removeNotification({ commit }, message) {
		commit('removeNotification', message)
	}
}
