module.exports = {
	important: true,
	theme: {
		screens: {
			ph: '320px',
			ta: '768px',
			de: '980px',
			hd: '1440px'
		},
		fontSize: {
			smallest: '0.625rem', // 10px
			smaller: '0.75rem', // 12px
			small: '0.875rem', // 14px

			default: '1rem', // 16px

			big: '1.125rem', // 18px
			bigger: '1.25rem', // 20px
			biggest: '1.5rem', // 24px

			large: '1.75rem', // 28px
			larger: '2rem', // 32px
			largest: '3.75rem', // 60px
			gigantic: '7.5rem' // 120px
		},
		colors: {
			transparent: 'transparent',
			dark: '#1e2021',
			main: '#1e2021',
			red: '#b45a5a',
			green: '#6b9d6e',
			dim: '#5b5f67',
			light: '#ffffff',
			accent: '#d6af89',
			highlight: '#d6af89',
			alt: '#929292'
		},
		letterSpacing: {
			tightest: '-0.09375rem', // -1.5px
			tighter: '-0.070625em', // -1.13px
			tight: '-0.0625rem', // - 1px
			'tight-less': '-0.03125rem', // -0.5px
			normal: '0',
			wide: '0.125rem', // 2px
			wider: '0.3125rem', // 5px
			widest: '0.625rem' // 10px
		},

		gridTemplateColumns: {
			'1': 'repeat(1, minmax(0, 1fr))',
			'2': 'repeat(2, minmax(0, 1fr))',
			'3': 'repeat(3, minmax(0, 1fr))',
			'4': 'repeat(4, minmax(0, 1fr))',
			'5': 'repeat(5, minmax(0, 1fr))',
			'2_75_25': 'minmax(0, .75fr) minmax(0, .25fr)',
			'2_25_75': 'minmax(0, .25fr) minmax(0, .75fr)',
			'2_66_33': 'minmax(0, .66fr) minmax(0, .34fr)',
			'2_33_66': 'minmax(0, .34fr) minmax(0, .66fr)',
			'2_50_50': 'minmax(0, .50fr) minmax(0, .50fr)'
		}
		// 	'75_25': '3 1',
		// 	'25_75': '1 3',
		// 	'50_50': '1 1',
		// 	'66_33': '2 1',
		// 	'33_66': '1 2'
		// }
	},
	plugins: []
}
