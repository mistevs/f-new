import Vue from 'vue'

import ContentSection from '~/components/content-section'
import Grid from '~/components/grid'
import Text from '~/components/txt'
import InviteForm from '~/components/form-invite'
import Video from '~/components/video'
import Hero from '~/components/hero'
import List from '~/components/list'
import ListItem from '~/components/list-item'
import SbImage from '~/components/sb-image'
import Gallery from '~/components/gallery'
import Parallax from '~/components/parallax'
import Partners from '~/components/partners'
import Card from '~/components/card'
import Button from '~/components/button'
import Tabs from '~/components/tabs'
import Tab from '~/components/tab'
import Attendees from '~/components/attendees'
import EventPassword from '~/components/event-password'
import EventRegister from '~/components/event-register'
import Code from '~/components/code'

Vue.component('BlokContentSection', ContentSection)
Vue.component('BlokGrid', Grid)
Vue.component('BlokText', Text)
Vue.component('BlokInviteForm', InviteForm)
Vue.component('BlokVideo', Video)
Vue.component('BlokHero', Hero)
Vue.component('BlokList', List)
Vue.component('BlokListItem', ListItem)
Vue.component('BlokImage', SbImage)
Vue.component('BlokGallery', Gallery)
Vue.component('BlokParallax', Parallax)
Vue.component('BlokPartners', Partners)
Vue.component('BlokCard', Card)
Vue.component('BlokButton', Button)
Vue.component('BlokTabs', Tabs)
Vue.component('BlokTab', Tab)
Vue.component('BlokAttendees', Attendees)
Vue.component('BlokEventPassword', EventPassword)
Vue.component('BlokEventRegister', EventRegister)
Vue.component('BlokCode', Code)
