const axios = require('axios')

export default {
	target: 'static',
	/*
	 ** Headers of the page
	 */
	head: {
		title: 'Future Horizon',
		meta: [
			{ charset: 'utf-8' },
			{
				name: 'viewport',
				content:
					'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no'
			},
			{
				hid: 'description',
				name: 'description',
				content: `Future Horizon
				is a gathering to
				envision, build
				and celebrate
				a sustainable
				future.`
			},
			{
				name: 'theme-color',
				content: '#181818'
			},
			{
				name: 'msapplication-navbutton-color',
				content: '#181818'
			},
			{
				name: 'apple-mobile-web-app-status-bar-style',
				content: '#181818'
			},
			{
				name: 'twitter:card',
				content: 'summary_large_image'
			},
			{
				name: 'twitter:site',
				content: '@futurehorizon'
			},
			{
				name: 'twitter:title',
				content: 'Future Horizon'
			},
			{
				name: 'twitter:description',
				content: `Future Horizon
				is a gathering to
				envision, build
				and celebrate
				a sustainable
				future.`
			},
			{
				name: 'twitter:image',
				content: 'https://f.camp/images/og-logo.png'
			},
			{
				property: 'og:url',
				content: 'https://futurehorizon.to'
			},
			{
				property: 'og:type',
				content: 'website'
			},
			{
				property: 'og:title',
				content: 'Future Horizon'
			},
			{
				property: 'og:description',
				content: `Future Horizon
				is a gathering to
				envision, build
				and celebrate
				a sustainable
				future.`
			},
			{
				property: 'og:image',
				content: 'https://futurehorizon.to/images/og-logo.png'
			},
			{
				property: 'og:image:width',
				content: '1200'
			},
			{
				property: 'og:image:height',
				content: '628'
			},
			{
				property: 'og:image:alt',
				content: 'Future Horizon Invitation'
			},
			{
				name: 'pinterest',
				content: 'nopin'
			}
		],
		link: [
			{
				rel: 'preload',
				type: 'image/jpg',
				as: 'image',
				href: '/images/events/covers/FH20221.jpg'
			},
			{
				rel: 'preload',
				type: 'image/jpg',
				as: 'image',
				href: '/images/events/covers/FH20211.jpg'
			},
			{
				rel: 'preload',
				type: 'image/jpg',
				as: 'image',
				href: '/images/events/covers/FH20202.jpg'
			},
			{
				rel: 'preload',
				type: 'image/jpg',
				as: 'image',
				href: '/images/events/covers/FH20201.jpg'
			},
			{
				rel: 'preload',
				type: 'image/jpg',
				as: 'image',
				href: '/images/events/covers/F2019.jpg'
			},
			{
				rel: 'preload',
				type: 'image/jpg',
				as: 'image',
				href: '/images/events/covers/F2018.jpg'
			},
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
			{
				href:
					'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap',
				rel: 'stylesheet'
			}
		],
		script: [
			{
				src: 'https://kit.fontawesome.com/61e2160178.js',
				crossorigin: 'anonymous'
			}
		]
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: { color: '#fff' },
	/*
	 ** Global CSS
	 */
	css: ['@/assets/scss/style.scss'],

	styleResources: {
		scss: [
			'./assets/scss/settings/_settings.scss',
			'./assets/scss/tools/_tools.scss'
		]
	},
	components: true,
	router: {
		middleware: 'setCacheVersion',
		trailingSlash: undefined
	},
	/*
	 ** Plugins to load before mounting the App
	 */
	plugins: ['~/plugins/components', '~/plugins/filters'],
	/*
	 ** Nuxt.js dev-modules
	 */
	buildModules: [
		// Doc: https://github.com/nuxt-community/eslint-module
		'@nuxtjs/eslint-module',
		// Doc: https://github.com/nuxt-community/nuxt-tailwindcss
		'@nuxtjs/tailwindcss'
	],
	/*
	 ** Nuxt.js modules
	 */
	modules: [
		// Doc: https://axios.nuxtjs.org/usage
		'@nuxtjs/axios',
		'@nuxtjs/pwa',
		'@nuxtjs/style-resources',
		'cookie-universal-nuxt',
		// Doc: https://github.com/nuxt-community/dotenv-module
		'@nuxtjs/dotenv',
		[
			'storyblok-nuxt',
			{
				accessToken: 'GGErSfvA8SowCAn0R51PNQtt',
				cacheProvider: 'memory'
			}
		]
	],
	/*
	 ** Axios module configuration
	 ** See https://axios.nuxtjs.org/options
	 */
	axios: {},
	/*
	 ** Build configuration
	 */
	build: {
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {
			config.module.rules.push({
				test: /\.(vert|frag)$/i,
				use: 'raw-loader'
			})
		}
	},

	/*
	 ** Using Links API
	 */
	generate: {
		routes: function(callback) {
			const token = `9ruJJLdZTkfsoMtV6uC3rwtt`
			const version = 'published'
			let cache_version = 0

			// other routes that are not in Storyblok with their slug.
			let routes = ['/'] // adds / directly

			// Load space and receive latest cache version key to improve performance
			axios
				.get(
					`https://api.storyblok.com/v1/cdn/spaces/me?token=${token}`
				)
				.then(space_res => {
					// timestamp of latest publish
					cache_version = space_res.data.space.version

					// Call for all Links using the Links API: https://www.storyblok.com/docs/Delivery-Api/Links
					axios
						.get(
							`https://api.storyblok.com/v1/cdn/links?token=${token}&version=${version}&cv=${cache_version}`
						)
						.then(res => {
							// console.log(res.data.links);
							Object.keys(res.data.links).forEach(key => {
								if (res.data.links[key].slug != 'home') {
									routes.push('/' + res.data.links[key].slug)
								}
							})

							callback(null, routes)
						})
				})
		}
	}
}
