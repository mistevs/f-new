module.exports = {
	root: true,
	env: {
		node: true,
		browser: true
	},
	extends: [
		'plugin:vue/recommended',
		'eslint:recommended',
		'prettier/vue',
		'plugin:prettier/recommended'
	],
	globals: {
		$nuxt: true
	},
	parserOptions: {
		parser: 'babel-eslint'
	},
	plugins: ['prettier'],
	// add your custom rules here
	rules: {
		'vue/no-v-html': 'off',
		'vue/multi-word-component-names': 'off',
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
	}
}
