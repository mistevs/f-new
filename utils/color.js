// hsv to rgb
export function HSVtoRGB(h, s, v) {
	let r, g, b

	if (h && s === undefined && v === undefined) {
		s = h.s
		v = h.v
		h = h.h
	}
	const i = Math.floor(h * 6)
	const f = h * 6 - i
	const p = v * (1 - s)
	const q = v * (1 - f * s)
	const t = v * (1 - (1 - f) * s)

	switch (i % 6) {
		case 0:
			r = v
			g = t
			b = p
			break
		case 1:
			r = q
			g = v
			b = p
			break
		case 2:
			r = p
			g = v
			b = t
			break
		case 3:
			r = p
			g = q
			b = v
			break
		case 4:
			r = t
			g = p
			b = v
			break
		case 5:
			r = v
			g = p
			b = q
			break
	}

	return {
		r,
		g,
		b
	}
}

export function convertHue(hue) {
	hue /= 360.0
	if (hue < 0) hue += 1.0
	return hue
}

export function componentToHex(c) {
	const hex = Math.round(c * 255).toString(16)
	return hex.length === 1 ? '0' + hex : hex
}

export function rgbToHex(c) {
	return '#' + componentToHex(c.r) + componentToHex(c.g) + componentToHex(c.b)
}
