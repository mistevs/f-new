import { gsap } from 'gsap'
import Screen from './screen'

export default class BG {
	constructor(el) {
		this.DOM = {
			el
		}
	}

	start(screen, cl) {
		if (!screen) {
			screen = new Screen(document.querySelector('.screen__item.' + cl))
		}

		// console.log(screen)

		gsap.timeline()
			.set(screen.DOM.el, { opacity: 1, zIndex: 1 })
			.to(screen.DOM.full, {
				duration: 4,
				ease: 'Power2.easeOut',
				startAt: { scale: 1.17 },
				scale: 1
			})
	}
}
