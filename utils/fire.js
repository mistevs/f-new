// import { gsap } from 'gsap'
import {
	// setupWebGL,
	// create3DContext,
	getWebGLContext,
	// loadShader,
	compileShader,
	createProgram
} from './fire.webgl.js'

// import GL from './gl-obj'

import {
	randomSpread,
	random2DVec,
	scaleVec,
	randomUnitVec,
	clone2DVec,
	unitVec,
	addVecs
} from './math'

import { HSVtoRGB, convertHue } from './color'
import { Noise } from './perlin'

import vert from '~/utils/shaders/fire.vert'
import frag from '~/utils/shaders/fire.frag'

const noise = new Noise()

// console.log('noise', Grad, noise, noise.seed)

export default class Fire {
	constructor(canvas, options) {
		this.options = {
			...{
				// this option is not actually in the UI
				fireEmitPositionSpread: { x: 100, y: 20 },

				fireEmitRate: 1600,
				fireEmitRateSlider: { min: 1, max: 5000 },

				fireSize: 40.0,
				fireSizeSlider: { min: 2.0, max: 100.0 },

				fireSizeVariance: 100.0,
				fireSizeVarianceSlider: { min: 0.0, max: 100.0 },

				fireEmitAngleVariance: 0.42,
				fireEmitAngleVarianceSlider: { min: 0.0, max: Math.PI / 2 },

				fireSpeed: 200.0,
				fireSpeedSlider: { min: 20.0, max: 500 },

				fireSpeedVariance: 80.0,
				fireSpeedVarianceSlider: { min: 0.0, max: 100.0 },

				fireDeathSpeed: 0.003,
				fireDeathSpeedSlider: { min: 0.001, max: 0.05 },

				fireTriangleness: 0.00015,
				fireTrianglenessSlider: { min: 0.0, max: 0.0003 },

				fireTextureHue: 25.0,
				fireTextureHueSlider: { min: -180, max: 180 },

				fireTextureHueVariance: 15.0,
				fireTextureHueVarianceSlider: { min: 0.0, max: 180 },

				fireTextureColorize: true,
				wind: true,
				omnidirectionalWind: false,

				windStrength: 20.0,
				windStrengthSlider: { min: 0.0, max: 60.0 },

				windTurbulance: 0.0003,
				windTurbulanceSlider: { min: 0.0, max: 0.001 },

				sparks: true,

				// some of these options for sparks are not actually available in the UI to save UI space
				sparkEmitRate: 6.0,
				sparkEmitSlider: { min: 0.0, max: 10.0 },

				sparkSize: 10.0,
				sparkSizeSlider: { min: 5.0, max: 100.0 },

				sparkSizeVariance: 20.0,
				sparkSizeVarianceSlider: { min: 0.0, max: 100.0 },

				sparkSpeed: 400.0,
				sparkSpeedSlider: { min: 20.0, max: 700.0 },

				sparkSpeedVariance: 80.0,
				sparkSpeedVarianceSlider: { min: 0.0, max: 100.0 },

				sparkDeathSpeed: 0.0085,
				sparkDeathSpeedSlider: { min: 0.002, max: 0.05 }
			},
			...options
		}

		console.log('enabling fire with options', this.options)

		this.textureList = [
			'heart.png'
			// 'rectangle.png',
			// 'circle.png',
			// 'gradient.png',
			// 'thicker_gradient.png',
			// 'explosion.png',
			// 'flame.png',
			// 'smilie.png'
		]

		this.textureIndex = 0

		this.textures = []
		this.images = []

		this.fireParticles = []
		this.sparkParticles = []

		this.frameTime = 18
		this.lastTime = this.time()
		this.lastFPSDivUpdate = this.time()

		this.particleDiscrepancy = 0
		this.lastParticleTime = this.time()
		this.sparkParticleDiscrepancy = 0

		noise.seed(Math.random())

		this.canvas = canvas
		this.gl = getWebGLContext(canvas)
		// this.gl = new GL(this.canvas, null, vert, frag)

		this.currentTime = 0
		this.lastFPSDivUpdate = 0
		this.frameTime = 0
		this.fps = 0
		this.lastTime = 0

		this.particleAverage = { x: 0, y: 0 }

		this.resolutionLocation = ''
		this.cameraLocation = ''
		this.textureSamplerLocation = ''

		this.positionAttrib = ''
		this.colorAttrib = ''
		this.textureCoordAttribute = ''

		this.rectArray = []
		this.colorArray = []
		this.textureCoordinates = []
	}

	createFireParticle(emitCenter) {
		const size = randomSpread(
			this.options.fireSize,
			this.options.fireSize * (this.options.fireSizeVariance / 100.0)
		)
		const speed = randomSpread(
			this.options.fireSpeed,
			(this.options.fireSpeed * this.options.fireSpeedVariance) / 100.0
		)
		let color = {}
		if (!this.options.fireTextureColorize)
			color = { r: 1.0, g: 1.0, b: 1.0, a: 0.5 }
		else {
			const hue = randomSpread(
				this.options.fireTextureHue,
				this.options.fireTextureHueVariance
			)
			color = HSVtoRGB(convertHue(hue), 1.0, 1.0)
			color.a = 0.5
		}
		const particle = {
			pos: random2DVec(emitCenter, this.options.fireEmitPositionSpread),
			vel: scaleVec(
				randomUnitVec(Math.PI / 2, this.options.fireEmitAngleVariance),
				speed
			),
			size: { width: size, height: size },
			color
		}
		this.fireParticles.push(particle)
	}

	createSparkParticle(emitCenter) {
		const size = randomSpread(
			this.options.sparkSize,
			this.options.sparkSize * (this.options.sparkSizeVariance / 100.0)
		)
		const origin = clone2DVec(emitCenter)
		const speed = randomSpread(
			this.options.sparkSpeed,
			(this.options.sparkSpeed * this.options.sparkSpeedVariance) / 100.0
		)
		const particle = {
			origin,
			pos: random2DVec(emitCenter, this.options.fireEmitPositionSpread),
			vel: scaleVec(
				randomUnitVec(
					Math.PI / 2,
					this.options.fireEmitAngleVariance * 2.0
				),
				speed
			),
			size: { width: size, height: size },
			color: { r: 1.0, g: 0.8, b: 0.3, a: 1.0 }
		}
		this.sparkParticles.push(particle)
	}

	canvasCoordinates(canvas, pos) {
		const rect = canvas.getBoundingClientRect()
		return { x: pos.x - rect.left, y: pos.y - rect.top }
	}

	loadAllTextures() {
		// image.src = 'textures/' + this.textureList[this.textureIndex]
		this.loadTexture(
			'textures/' + this.textureList[this.textureIndex],
			this.textureIndex
		)
	}

	loadTexture(textureName, index) {
		this.textures[index] = this.gl.createTexture()
		this.images[index] = new Image()
		this.images[index].src = textureName
		this.images[index].onload = () => {
			console.log(
				'loading texture ...',
				index,
				textureName,
				this.images[index]
			)
			this.handleTextureLoaded(this.images[index], index, textureName)
		}

		this.images[index].onerror = function() {
			alert('ERROR: texture ' + textureName + " can't be loaded!")
			console.error('ERROR: texture ' + textureName + " can't be loaded!")
		}

		// this.images[index].src = textureName
		console.log('starting to load ' + textureName)
	}

	handleTextureLoaded(image, index, textureName) {
		console.log('loaded texture ', textureName, this.textures[index])

		this.gl.bindTexture(this.gl.TEXTURE_2D, this.textures[index])
		this.gl.texImage2D(
			this.gl.TEXTURE_2D,
			0,
			this.gl.RGBA,
			this.gl.RGBA,
			this.gl.UNSIGNED_BYTE,
			image
		)

		this.gl.texParameteri(
			this.gl.TEXTURE_2D,
			this.gl.TEXTURE_MAG_FILTER,
			this.gl.LINEAR
		)

		this.gl.texParameteri(
			this.gl.TEXTURE_2D,
			this.gl.TEXTURE_MIN_FILTER,
			this.gl.LINEAR_MIPMAP_NEAREST
		)

		this.gl.generateMipmap(this.gl.TEXTURE_2D)
		this.gl.bindTexture(this.gl.TEXTURE_2D, null)

		// load the next texture
		// if (index < this.textureList.length - 1)
		// 	this.loadTexture(
		// 		'textures/' + this.textureList[index + 1],
		// 		index + 1
		// 	)
		// texturesLoadedCount += 1;
	}

	markForDeletion(array, index) {
		array[index] = undefined
	}

	deleteMarked(array) {
		let newIndex = 0
		for (let i = 0; i < array.length; i++) {
			if (array[i] !== undefined) {
				array[newIndex] = array[i]
				newIndex += 1
			}
		}
		return array.slice(0, newIndex)
	}

	start() {
		if (!this.gl) {
			return
		}

		this.loadAllTextures()

		const tex = this.gl.createTexture()
		this.gl.bindTexture(this.gl.TEXTURE_2D, tex)
		this.gl.texImage2D(
			this.gl.TEXTURE_2D,
			0,
			this.gl.RGBA,
			1,
			1,
			0,
			this.gl.RGBA,
			this.gl.UNSIGNED_BYTE,
			new Uint8Array([255, 0, 0, 255])
		) // red

		this.gl.clearColor(0.0, 0.0, 0.0, 0.0)

		this.vertexBuffer = this.gl.createBuffer()
		this.colorBuffer = this.gl.createBuffer()
		this.squareTextureCoordinateVertices = this.gl.createBuffer()

		console.log('compiling shaders', vert, frag)

		this.vertexShader = compileShader(this.gl, this.gl.VERTEX_SHADER, vert)
		this.fragmentShader = compileShader(
			this.gl,
			this.gl.FRAGMENT_SHADER,
			frag
		)

		this.program = createProgram(this.gl, [
			this.vertexShader,
			this.fragmentShader
		])

		this.gl.useProgram(this.program)

		console.log('program', this.program)

		// look up where the vertex data needs to go.
		this.positionAttrib = this.gl.getAttribLocation(
			this.program,
			'a_position'
		)
		this.gl.enableVertexAttribArray(this.positionAttrib)
		this.colorAttrib = this.gl.getAttribLocation(this.program, 'a_color')
		this.gl.enableVertexAttribArray(this.colorAttrib)
		this.textureCoordAttribute = this.gl.getAttribLocation(
			this.program,
			'a_texture_coord'
		)
		this.gl.enableVertexAttribArray(this.textureCoordAttribute)

		// setup GLSL program

		// look up where the vertex data needs to go.
		this.positionAttrib = this.gl.getAttribLocation(
			this.program,
			'a_position'
		)

		console.log('positionAttrib', this.positionAttrib)

		this.gl.enableVertexAttribArray(this.positionAttrib)
		this.colorAttrib = this.gl.getAttribLocation(this.program, 'a_color')

		console.log('colorAttrib', this.colorAttrib)

		this.gl.enableVertexAttribArray(this.colorAttrib)
		this.textureCoordAttribute = this.gl.getAttribLocation(
			this.program,
			'a_texture_coord'
		)

		console.log('textureCoordAttribute', this.textureCoordAttribute)
		this.gl.enableVertexAttribArray(this.textureCoordAttribute)

		// // lookup uniforms
		this.resolutionLocation = this.gl.getUniformLocation(
			this.program,
			'u_resolution'
		)
		this.cameraLocation = this.gl.getUniformLocation(
			this.program,
			'cam_position'
		)
		this.textureSamplerLocation = this.gl.getUniformLocation(
			this.program,
			'u_sampler'
		)

		this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE)
		this.gl.enable(this.gl.BLEND)
		this.gl.texParameteri(
			this.gl.TEXTURE_2D,
			this.gl.TEXTURE_MIN_FILTER,
			this.gl.LINEAR
		)
		this.gl.texParameteri(
			this.gl.TEXTURE_2D,
			this.gl.TEXTURE_WRAP_S,
			this.gl.CLAMP_TO_EDGE
		) // Prevents s-coordinate wrapping (repeating).
		this.gl.texParameteri(
			this.gl.TEXTURE_2D,
			this.gl.TEXTURE_WRAP_T,
			this.gl.CLAMP_TO_EDGE
		) // Prevents t-coordinate wrapping (repeating).

		this.animloop()
	}

	// main program loop
	animloop() {
		requestAnimationFrame(this.animloop.bind(this))
		this.timing()
		this.logic()
		this.render()
	}

	// the timing function's only job is to calculate the framerate

	timing() {
		this.currentTime = this.time()
		this.frameTime =
			this.frameTime * 0.9 + (this.currentTime - this.lastTime) * 0.1
		this.fps = 1000.0 / this.frameTime
		if (this.currentTime - this.lastFPSDivUpdate > 100) {
			const e = document.getElementById('fps')
			if (e) e.innerHTML = 'FPS: ' + Math.round(this.fps)

			this.lastFPSDivUpdate = this.currentTime
		}
		this.lastTime = this.currentTime
	}

	time() {
		const d = new Date()
		const n = d.getTime()
		return n
	}

	// calculate new positions for all the particles
	logic() {
		const currentParticleTime = this.time()
		let timeDifference = currentParticleTime - this.lastParticleTime

		// we don't want to generate a ton of particles if the browser was minimized or something
		if (timeDifference > 100) timeDifference = 100

		// update fire particles

		this.particleDiscrepancy +=
			(this.options.fireEmitRate * timeDifference) / 1000.0
		while (this.particleDiscrepancy > 0) {
			this.createFireParticle({
				x: this.canvas.width / 2,
				y: this.canvas.height / 2 + 200
			})
			this.particleDiscrepancy -= 1.0
		}

		const numParts = this.fireParticles.length
		for (let i = 0; i < numParts; i++) {
			this.particleAverage.x += this.fireParticles[i].pos.x / numParts
			this.particleAverage.y += this.fireParticles[i].pos.y / numParts
		}

		for (let i = 0; i < this.fireParticles.length; i++) {
			const x = this.fireParticles[i].pos.x
			const y = this.fireParticles[i].pos.y

			// apply wind to the velocity
			if (this.options.wind) {
				if (this.options.omnidirectionalWind)
					this.fireParticles[i].vel = addVecs(
						this.fireParticles[i].vel,
						scaleVec(
							unitVec(
								(noise.simplex3(
									x / 500,
									y / 500,
									this.lastParticleTime *
										this.options.windTurbulance
								) +
									1.0) *
									Math.PI
							),
							this.options.windStrength
						)
					)
				else
					this.fireParticles[i].vel = addVecs(
						this.fireParticles[i].vel,
						scaleVec(
							unitVec(
								(noise.simplex3(
									x / 500,
									y / 500,
									this.lastParticleTime *
										this.options.windTurbulance
								) +
									1.0) *
									Math.PI *
									0.5
							),
							this.options.windStrength
						)
					)
			}
			// move the particle
			this.fireParticles[i].pos = addVecs(
				this.fireParticles[i].pos,
				scaleVec(this.fireParticles[i].vel, timeDifference / 1000.0)
			)

			// var offAngle = angleBetweenVecs(fireParticles[i].vel,subVecs(particleAverage,));
			// console.log(offAngle);
			this.fireParticles[i].color.a -=
				this.options.fireDeathSpeed +
				Math.abs(this.particleAverage.x - this.fireParticles[i].pos.x) *
					this.options.fireTriangleness // ;Math.abs((fireParticles[i].pos.x-canvas.width/2)*options.fireTriangleness);

			if (
				this.fireParticles[i].pos.y <=
					-this.fireParticles[i].size.height * 2 ||
				this.fireParticles[i].color.a <= 0
			)
				this.markForDeletion(this.fireParticles, i)
		}

		this.fireParticles = this.deleteMarked(this.fireParticles)

		// update spark particles
		this.sparkParticleDiscrepancy +=
			(this.options.sparkEmitRate * timeDifference) / 1000.0
		while (this.sparkParticleDiscrepancy > 0) {
			this.createSparkParticle({
				x: this.canvas.width / 2,
				y: this.canvas.height / 2 + 200
			})
			this.sparkParticleDiscrepancy -= 1.0
		}

		for (let i = 0; i < this.sparkParticles.length; i++) {
			const x = this.sparkParticles[i].pos.x
			const y = this.sparkParticles[i].pos.y
			this.sparkParticles[i].vel = addVecs(
				this.sparkParticles[i].vel,
				scaleVec(
					unitVec(
						(noise.simplex3(
							x / 500,
							y / 500,
							this.lastParticleTime * 0.0003
						) +
							1.0) *
							Math.PI *
							0.5
					),
					20.0
				)
			)

			this.sparkParticles[i].pos = addVecs(
				this.sparkParticles[i].pos,
				scaleVec(this.sparkParticles[i].vel, timeDifference / 1000.0)
			)

			this.sparkParticles[i].color.a -= this.options.sparkDeathSpeed

			if (
				this.sparkParticles[i].pos.y <=
					-this.sparkParticles[i].size.height * 2 ||
				this.sparkParticles[i].color.a <= 0
			)
				this.markForDeletion(this.sparkParticles, i)
		}

		this.sparkParticles = this.deleteMarked(this.sparkParticles)
		const e = document.getElementById('numParticles')
		if (e)
			e.innerHTML =
				'# particles: ' +
				(this.fireParticles.length + this.sparkParticles.length)

		this.lastParticleTime = currentParticleTime
	}

	render() {
		this.gl.clear(this.gl.COLOR_BUFFER_BIT)
		// set the resolution

		// console.log(
		// 	'rendering ...',
		// 	this.resolutionLocation,
		// 	this.canvas.width,
		// 	this.canvas.height
		// )

		this.gl.uniform2f(
			this.resolutionLocation,
			this.canvas.width,
			this.canvas.height
		)

		this.gl.uniform1i(this.textureSamplerLocation, 0)

		// this.drawRects(this.fireParticles)
		// if (this.options.sparks) {
		this.drawRects(this.sparkParticles)
		// }

		// console.log('particle avg', this.particleAverage)
	}

	concatInPlace(index, arr1, arr2) {
		for (let i = 0; i < arr2.length; i++) {
			arr1[index] = arr2[i]
			index += 1
		}
		return index
	}

	drawRects(rects, textureIndex) {
		let index = 0
		let colorIndex = 0
		let texIndex = 0
		this.rectArray = []
		this.colorArray = []
		this.textureCoordinates = []

		for (let i = 0; i < rects.length; i++) {
			const x1 = rects[i].pos.x - rects[i].size.width / 2
			const x2 = rects[i].pos.x + rects[i].size.width / 2
			const y1 = rects[i].pos.y - rects[i].size.height / 2
			const y2 = rects[i].pos.y + rects[i].size.height / 2
			index = this.concatInPlace(index, this.rectArray, [
				x1,
				y1,
				x2,
				y1,
				x1,
				y2,
				x1,
				y2,
				x2,
				y1,
				x2,
				y2
			])
			texIndex = this.concatInPlace(texIndex, this.textureCoordinates, [
				0.0,
				0.0,
				1.0,
				0.0,
				0.0,
				1.0,
				0.0,
				1.0,
				1.0,
				0.0,
				1.0,
				1.0
			])
			for (let ii = 0; ii < 6; ii++) {
				colorIndex = this.concatInPlace(colorIndex, this.colorArray, [
					rects[i].color.r,
					rects[i].color.g,
					rects[i].color.b,
					rects[i].color.a
				])
			}
		}

		// console.log(
		// 	'drawing gl',
		// 	this.gl.TEXTURE0,
		// 	this.textures[this.textureIndex]
		// )

		this.gl.activeTexture(this.gl.TEXTURE0)
		this.gl.bindTexture(
			this.gl.TEXTURE_2D,
			this.textures[this.textureIndex]
		)

		this.gl.bindBuffer(
			this.gl.ARRAY_BUFFER,
			this.squareTextureCoordinateVertices
		)
		this.gl.vertexAttribPointer(
			this.textureCoordAttribute,
			2,
			this.gl.FLOAT,
			false,
			0,
			0
		)
		this.gl.bufferData(
			this.gl.ARRAY_BUFFER,
			new Float32Array(this.textureCoordinates),
			this.gl.STATIC_DRAW
		)

		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer)
		this.gl.vertexAttribPointer(
			this.positionAttrib,
			2,
			this.gl.FLOAT,
			false,
			0,
			0
		)
		this.gl.bufferData(
			this.gl.ARRAY_BUFFER,
			new Float32Array(this.rectArray),
			this.gl.STATIC_DRAW
		)

		this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.colorBuffer)
		this.gl.vertexAttribPointer(
			this.colorAttrib,
			4,
			this.gl.FLOAT,
			false,
			0,
			0
		)
		this.gl.bufferData(
			this.gl.ARRAY_BUFFER,
			new Float32Array(this.colorArray),
			this.gl.STATIC_DRAW
		)

		this.gl.drawArrays(this.gl.TRIANGLES, 0, rects.length * 6)
	}
}
