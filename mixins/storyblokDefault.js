export default {
	data() {
		return { story: { content: {} } }
	},

	async asyncData(context) {
		// Check if we are in the editing mode
		// console.log(
		// 	'async data from SB default',
		// 	context.query._storyblok,
		// 	context.isDev
		// )

		let editMode = false

		console.log('loaded asyncdata in storyblok default', context.query)

		if (
			context.query._storyblok ||
			context.isDev ||
			(typeof window !== 'undefined' &&
				window.localStorage.getItem('_storyblok_draft_mode'))
		) {
			if (typeof window !== 'undefined') {
				window.localStorage.setItem('_storyblok_draft_mode', '1')
				if (window.location === window.parent.location) {
					window.localStorage.removeItem('_storyblok_draft_mode')
				}
			}

			editMode = true
		}

		const version = editMode ? 'draft' : 'published'
		const path =
			context.route.path === '/' || !context.route.path
				? '/home'
				: context.route.path
		const fullPath = `cdn/stories${path}`

		// console.log('loading stories from ', fullPath)
		// Load the JSON from the API
		const { data } = await context.app.$storyapi.get(fullPath, {
			version,
			cv: context.store.state.cacheVersion
		})

		// console.log('received stories ', data)

		/* .then((res) => {
			// console.log('received response', res.data.stories[0])
			return res.data
		}).catch((res) => {
			if (!res.response) {
				console.error(res)
				context.error({ statusCode: 404, message: 'Failed to receive content form api' })
			} else {
				console.error(res.response.data)
				context.error({ statusCode: res.response.status, message: res.response.data })
			}
		}); */
		// console.log('story from default', data.story);

		return data
	}
}
