export default {
	methods: {
		getImagePath(width, height, image) {
			if (image || (this.blok && this.blok.image)) {
				return (
					'//img2.storyblok.com/' +
					width +
					'x' +
					height +
					(image && typeof image !== 'undefined'
						? image
						: this.blok.image
					).replace('//a.storyblok.com', '')
				)
			} else {
				return ''
			}
		},

		getDate(d) {
			// const parts = d.split(' ')
			// const date = parts[0]
			// const time = parts[1]

			// const dateParts = date.split('-')
			// const year = dateParts[0]
			// const month = dateParts[1]
			// const day = dateParts[2]

			// const string = month + '-' + day + '-' + year + 'T' + time
			// console.log(d + ' to ' + string)
			return new Date(d.replace(/ /g, 'T'))
		}
	}
}
