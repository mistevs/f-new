export default {
	mounted() {
		// if (window.location.search.includes('_storyblok')) {
		this.loadBridge(this.registerBridge)
	},

	methods: {
		loadBridge(callback) {
			const existingScript = document.getElementById('storyblokBridge')
			// console.log('!! LOADING STORYBLOCK BRIDGE !!')
			if (!existingScript) {
				// console.log('!! LOADING STORYBLOCK BRIDGE !! :: No Script')
				const script = document.createElement('script')
				script.src = '//app.storyblok.com/f/storyblok-v2-latest.js'
				script.id = 'storyblokBridge'
				document.head.appendChild(script)
				script.onload = () => {
					callback()
				}
			} else {
				callback()
			}
		},

		registerBridge() {
			const { StoryblokBridge, location } = window
			if (!StoryblokBridge) {
				// console.log('!! STORYBLOCK REGISTER !! :: No Bridge')
				return false
			}

			const storyblokInstance = new StoryblokBridge()
			storyblokInstance.pingEditor(() => {
				if (storyblokInstance.isInEditor()) {
					//  load the draft version
				} else {
					// load the published version
				}
			})

			// console.log(
			// 	'!! STORYBLOCK REGISTERING !!',
			// 	storyblokInstance,
			// 	StoryblokBridge
			// )

			storyblokInstance.on(['input', 'published', 'change'], event => {
				// console.log('!! STORYBLOCK !! :: Heard Event', event)

				if (event.action === 'input') {
					if (event.story.id === this.story.id) {
						this.story.content = event.story.content
					}
				} else if (!event.slugChanged) {
					location.reload()
				}
			})
		}
	}
}
