import axios from 'axios'

export default class Endpoint {
	constructor(
		ax,
		baseUrl = 'https://api.fl0.com/mistevs/{{env}}/futurehorizon/flows'
	) {
		let env = 'dev'
		// if (
		// 	typeof window !== 'undefined' &&
		// 	window.location &&
		// 	window.location.href
		// ) {
		// 	if (window.location.href.includes('localhost')) env = 'dev'
		// }

		this.axios = ax || axios
		this.baseUrl = baseUrl.replace('{{env}}', env)
	}
}
