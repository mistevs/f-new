import Endpoint from '@/api/endpoint'

export default class User extends Endpoint {
	constructor() {
		super()

		this.baseUrl += '/user'
	}

	getUser(id) {
		// console.log('getting user', id)
		return this.axios.get(this.baseUrl, {
			params: {
				id
			}
		})
	}

	login(email, password) {
		if (!email || !password) throw new Error('Email or password missing.')

		// console.log(this.baseUrl, email, password)
		return this.axios.post(this.baseUrl + '/login', {
			email,
			password
		})
	}
	confirmResetPassword(token, password) {
		// console.log('saw confirm password request', token, password)
		if (!token || !password)
			throw new Error(
				'A token and password must be given in order to change password.'
			)
		return this.axios.post(this.baseUrl + '/confirm-reset-password', {
			token,
			password
		})
	}

	resetPassword(email) {
		if (!email)
			throw new Error(
				'An email address must be given in order to request a password reset.'
			)
		return this.axios.get(this.baseUrl + '/reset-password', {
			params: { email }
		})
	}

	register(Id, firstName, lastName, email, password) {
		if (!Id)
			throw new Error(
				'It seems the invite link you clicked was malformed.  If this problem persists, please contact Angela to send a new invitation.'
			)
		if (!firstName)
			throw new Error(
				'We need to know your name before we can create your account.'
			)

		if (!lastName)
			throw new Error(
				'We need to know your name before we can create your account.'
			)

		if (!email)
			throw new Error(
				"You'll need to supply an email address to log into with in the future."
			)

		if (!password)
			throw new Error(
				"You'll need to supply a password to login with in the future"
			)

		return this.axios.post(this.baseUrl + '/register', {
			Id,
			firstName,
			lastName,
			email,
			password
		})
	}

	createAccount(firstName, lastName, email, password) {
		if (!firstName)
			throw new Error(
				'We need to know your name before we can create your account.'
			)

		if (!lastName)
			throw new Error(
				'We need to know your name before we can create your account.'
			)

		if (!email)
			throw new Error(
				"You'll need to supply an email address to log into with in the future."
			)

		if (!password)
			throw new Error(
				"You'll need to supply a password to login with in the future"
			)

		return this.axios.post(this.baseUrl + '/create-account', {
			firstName,
			lastName,
			email,
			password
		})
	}

	requestInvite(email, org, event) {
		// return this.axios.post(this.baseUrl + '/request-invite', {
		// 	email,
		// 	org,
		// 	event
		// })
		return this.axios.post('https://fq-app-api-prod.vercel.app/api/services/sendInviteForm', {
			email,
		})
	}

	invite(
		event,
		firstName,
		lastName,
		email,
		template,
		promotionCode,
		promotionAmount
	) {
		if (!email || !event || !firstName)
			throw new Error('Email, first name, and event are required.')

		if (template.promo && !promotionCode && !promotionAmount)
			throw new Error('The template you chose requires a promo code.')

		const accountRegistrationBase =
			window.location.protocol + '//' + window.location.host + '/confirm'

		// console.log(
		// 	'inviting',
		// 	event,
		// 	firstName,
		// 	lastName,
		// 	email,
		// 	accountRegistrationBase
		// )

		return this.axios.post(this.baseUrl + '/invite', {
			email,
			firstName,
			lastName,
			event,
			template: template.id,
			promotionCode,
			promotionAmount,
			accountRegistrationBase
		})
	}

	getProfileImage(id) {
		// console.log('getting me profile')
		return this.axios.get(this.baseUrl + '/profile-image', {
			params: { id }
		})
	}
}
