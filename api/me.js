import Endpoint from '@/api/endpoint'

export default class Me extends Endpoint {
	constructor() {
		super()

		this.baseUrl += '/me'
	}

	getProfile() {
		// console.log('getting me profile')
		return this.axios.get(this.baseUrl + '/profile')
	}

	getProfileImage() {
		// console.log('getting me profile')
		return this.axios.get(this.baseUrl + '/profile-image')
	}

	saveProfile(
		firstName,
		lastName,
		bio,
		dateOfBirth,
		website,
		linkedin,
		instagram,
		twitter,
		discord
	) {
		// console.log(arguments)
		return this.axios.put(this.baseUrl + '/profile', {
			firstName,
			lastName,
			bio,
			dateOfBirth,
			website,
			linkedin,
			instagram,
			twitter,
			discord
		})
	}

	saveProfileImage(formData, userId) {
		const url = this.baseUrl + '/profile-image?_u=' + userId
		const req = this.axios
			.post(url, formData)
			// get data
			.then(x => x.data)
			// add url field
			.then(x =>
				x.map(img =>
					Object.assign({}, img, {
						url: `${this.baseUrl}/images/${img.id}`
					})
				)
			)
		return req
	}

	auth() {
		return this.axios.get(this.baseUrl)
	}

	events() {
		return this.axios.get(this.baseUrl + '/events')
	}

	joinEvent(event) {
		if (!event) throw new Error('No event was given.')

		return this.axios.post(this.baseUrl + '/join-event', {
			event
		})
	}
}
