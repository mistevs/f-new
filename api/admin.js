import Endpoint from '@/api/endpoint'

export default class Admin extends Endpoint {
	constructor() {
		super()

		this.baseUrl += '/admin'
	}

	searchUsers(key, value) {
		return this.axios.get(this.baseUrl + '/search-users', {
			params: {
				key,
				value
			}
		})
	}

	getUserProfile(id) {
		// console.log('getting me profile')
		return this.axios.get(this.baseUrl + '/user-profile', {
			params: { id }
		})
	}

	saveUserProfile(
		id,
		firstName,
		lastName,
		bio,
		dateOfBirth,
		website,
		linkedin,
		instagram,
		twitter,
		discord
	) {
		// console.log(arguments)
		return this.axios.put(this.baseUrl + '/profile', {
			id,
			firstName,
			lastName,
			bio,
			dateOfBirth,
			website,
			linkedin,
			instagram,
			twitter,
			discord
		})
	}

	saveUserProfileImage(formData, userId) {
		const url = this.baseUrl + '/profile-image?id=' + userId
		const req = this.axios
			.post(url, formData)
			// get data
			.then(x => x.data)
			// add url field
			.then(x =>
				x.map(img =>
					Object.assign({}, img, {
						url: `${this.baseUrl}/images/${img.id}`
					})
				)
			)
		return req
	}

	changeRole(id, role) {
		return this.axios.post(this.baseUrl + '/change-role', {
			id,
			role
		})
	}

	getUserEvents(id) {
		return this.axios.get(this.baseUrl + '/user-events', {
			params: {
				id
			}
		})
	}

	compEvent(id) {
		return this.axios.post(this.baseUrl + '/comp-user-event', {
			id
		})
	}

	addUserToEvent(id, event) {
		return this.axios.post(this.baseUrl + '/user-event', {
			id,
			event
		})
	}

	addUser(mem) {
		console.log('adding user ...', mem)
		return this.axios.post(this.baseUrl + '/add-user', mem)
	}

	// EVENT //

	editEvent(data) {
		return this.axios.put(this.baseUrl + '/event', { ...data })
	}
}
