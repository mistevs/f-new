import Endpoint from '@/api/endpoint'

export default class Event extends Endpoint {
	constructor() {
		super()

		this.baseUrl += '/event'
	}

	all() {
		return this.axios.get(`${this.baseUrl}s`)
	}

	get(id) {
		return this.axios.get(`${this.baseUrl}`, { params: { id } })
	}

	getAttendees(event) {
		return this.axios.get(`${this.baseUrl}/attendees`, {
			params: {
				event
			}
		})
	}
}
